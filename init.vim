" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
	sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
endif

call plug#begin()
Plug 'ycm-core/YouCompleteMe'
Plug 'lervag/vimtex'
Plug 'Vimjas/vim-python-pep8-indent'
Plug 'vim-syntastic/syntastic'
Plug 'alvan/vim-closetag'
Plug 'KabbAmine/vCoolor.vim'
call plug#end()

let mapleader=" "
let g:livepreview_previewer = 'zathura'

nnoremap <leader>n :set relativenumber!<CR>
nnoremap <leader>t :term<CR>
nnoremap <leader>w :w!<CR>
nnoremap <leader>q :qa!<CR>

" Basics
set number
syntax enable
set encoding=utf-8
set smartcase
set noswapfile
set incsearch

" Disable Arrow Keys
for key in ['<Up>', '<Down>', '<Left>', '<Right>']
  exec 'noremap' key '<Nop>'
  exec 'inoremap' key '<Nop>'
  exec 'cnoremap' key '<Nop>'
endfor

augroup SeriouslyNoInsertArrows
  autocmd!
  autocmd InsertEnter * inoremap <Up> <nop>
  autocmd InsertEnter * inoremap <Down> <nop>
augroup end

" Status bar
set statusline=
set statusline+=%#PmenuSel#
set statusline+=%{StatuslineGit()}
set statusline+=%#LineNr#
set statusline+=\ %f
set statusline+=%m\
set statusline+=%=
set statusline+=%#CursorColumn#
set statusline+=\ %y
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\[%{&fileformat}\]
set statusline+=\ %p%%
set statusline+=\ %l:%c
set statusline+=\

function! GitBranch()
  return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction

function! StatuslineGit()
  let l:branchname = GitBranch()
  return strlen(l:branchname) > 0?'  '.l:branchname.' ':''
endfunction

" Disable autocomment on new line
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Splits
set splitbelow splitright

" Autocompletion
set wildmode=longest,list,full

" 4 space tabs
set tabstop=4
set shiftwidth=4
set expandtab
set smartindent

" linebreak stuff
set wrap
set breakindent
set breakindentopt=sbr
set showbreak=->

colorscheme heman
set background=dark

" Disable vi stuff
set nocompatible

" Search down into subfolders
set path+=**

" Display all matching files when tab complete
set wildmenu
set wildignore+=*.pyc,*.o,*.obj,*.svn,*.swp,*.class,*.hg,*.DS_Store,*.min.*,*.meta

" Delete trailing whitespace on save
autocmd BufWritePre * %s/\s\+$//e

" Syntax error checks
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" Set template when opening new html file
augroup html
    autocmd! BufNewFile *.html 0r ~/.config/nvim/skeletons/pencom.html|normal 3j%l
    autocmd BufNewFile *.html startinsert
augroup end
